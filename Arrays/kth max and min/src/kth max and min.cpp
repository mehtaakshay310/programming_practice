//============================================================================
// Name        : kth.cpp
// Author      : Akshay Mehta
//============================================================================

#include <iostream>
#include <algorithm>
#include <climits>
using namespace std;


void print(int a[], int size) {
	for (int i = 0; i < size; ++i)
		cout << "element of a[" << i << "] = " << a[i] << endl;
}

void getMinMax(int *arr, int size,int maxk,int mink) {
	sort(arr,arr+size-1);
	print(arr,size);
	cout<<"Max : "<<arr[size-maxk]<<endl;
	cout<<"Min : "<<arr[mink-1]<<endl;
}

int main() {
	int size;
	cout << "Enter Size of Array : ";
	cin >> size;
	int a[size];
	for (int i = 0; i < size; ++i) {
		cout << "Enter element of a[" << i << "] = ";
		cin >> a[i];
	}
	cout << "\nArrays element";
	print(a, size);
	cout<<"Enter kth position for max and min";
	int mink,maxk;
	cin>>maxk>>mink;
	getMinMax(a, size,maxk,mink);
	return 0;
}
