//============================================================================
// Name        : Max_Min_Element.cpp
// Author      : Akshay Mehta
//============================================================================

#include <limits.h>
#include <iostream>
using namespace std;

void print(int a[], int size) {
	for (int i = 0; i < size; ++i)
		cout << "element of a[" << i << "] = " << a[i] << endl;
}

void getMinMax(int *arr, int size) {
	int min=INT_MAX;
	int max=INT_MIN;
	for (int i = 0; i < size; ++i) {
		if(min>arr[i])
			min=arr[i];
		if(max<arr[i])
			max=arr[i];
	}
	cout<<"Min : "<<min<<endl;
	cout<<"Max : "<<max<<endl;
}

int main() {
	int size;
	cout << "Enter Size of Array : ";
	cin >> size;
	int a[size];
	for (int i = 0; i < size; ++i) {
		cout << "Enter element of a[" << i << "] = ";
		cin >> a[i];
	}
	cout << "\nArrays element";
	print(a, size);
	getMinMax(a, size);
}
