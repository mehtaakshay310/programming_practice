//============================================================================
// Name        : Reverse_The_Array.cpp
// Author      : Akshay Mehta
//============================================================================

#include <iostream>
using namespace std;

void reverseArray(int a[],int size)
{
	for (int i = 0; i < size/2; ++i) {
		int temp=a[i];
		a[i]=a[size-i-1];
		a[size-i-1]=temp;
	}
}

void print(int a[],int size)
{
	for (int i = 0; i < size; ++i)
			cout<<"element of a["<<i<<"] = "<<a[i]<<endl;
}

int main() {
	int size;
	cout<<"Enter Size of Array : ";
	cin>>size;
	int a[size];
	for (int i = 0; i < size; ++i) {
		cout<<"Enter element of a["<<i<<"] = ";
		cin>>a[i];
	}
	cout<<"\nArrays element";
	print(a,size);
	reverseArray(a,size);
	cout<<"\nArray in Reverse Order"<<endl;

	print(a,size);
	return 0;
}
